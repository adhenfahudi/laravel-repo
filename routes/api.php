<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//CRUD PLAYER Table
Route::get('/team', 'TeamController@index');
Route::get('/player','PlayerController@indexplayer');
// Route::get('/pekerja/tambah','PekerjaController@tambah');
Route::post('/player/store','PlayerController@storeplayer');
Route::get('/player/getid/{id}','PlayerController@getidplayer');
Route::put('/player/update/{id}','PlayerController@updateplayer');
Route::delete('/player/delete/{id}', 'PlayerController@deleteplayer');


// CRUD Pekerja Table
Route::group(['middleware' => 'auth.jwt'], function () {
Route::get('/pekerja', 'PekerjaController@index');
Route::post('/pekerja/store','PekerjaController@store');
Route::get('/pekerja/getid/{id}','PekerjaController@getid');
Route::put('/pekerja/update/{id}','PekerjaController@update');
Route::delete('/pekerja/delete/{id}','PekerjaController@delete'); 
});

//CRUD Authentication Method
// Route::get('/pekerja', ['middleware' => 'jwt.auth', 'uses' => 'PekerjaController@index']);
Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');
 //Route::middleware('jwt.auth')->group(function(){
    

//});