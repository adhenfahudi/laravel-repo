<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #23 : Relasi One To One Eloquent</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
 
	<div class="container">
		<div class="card mt-5">
			<div class="card-body">
				<h3 class="text-center"><a href="https://www.malasngoding.com">MBAHGANTENG.BLOGSPOT.COM</a></h3>
				<h5 class="text-center my-4">Eloquent One To One Relationship</h5>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Nama Team</th>
                            <th>Nama Content Creator</th>
                            <th>Nama Player</th>
                            <th>Nama Pekerja</th>
                            <th>Nama Cabang</th>
                            <th> </th>
						</tr>
					</thead>
					<tbody>
						@foreach($team as $p)
						<tr>
							<td>{{ $p->team_id }}</td>
                            <td>{{ $p->pekerja->nama}}</td>
                            <td>{{ $p->concreate->nama}}</td>
                            <td>{{ $p->player->nama}}</td>
                            <td>{{ $p->division->nama_cabang}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
 
</body>
</html>