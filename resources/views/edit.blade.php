<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Membuat CRUD Pada Laravel - www.malasngoding.com</title>
</head>
<body>
 
	<h2><a href="https://www.malasngoding.com">www.malasngoding.com</a></h2>
	<h3>Edit Pegawai</h3>
 
	<a href="/testproject/public/pekerja"> Kembali</a>
	
	<br/>
	<br/>
 
	@foreach($pekerja as $p)
	<form action="/testproject/public/pekerja/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $p->pekerja_id }}"> <br/>
		Nama <input type="text" required="required" name="nama" value="{{ $p->pekerja_nama }}"> <br/>
		Jabatan <input type="text" required="required" name="jabatan" value="{{ $p->pekerja_jabatan }}"> <br/>
		Umur <input type="number" required="required" name="umur" value="{{ $p->pekerja_umur }}"> <br/>
		Alamat <textarea required="required" name="alamat">{{ $p->pekerja_alamat }}</textarea> <br/>
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach
		
 
</body>
</html>