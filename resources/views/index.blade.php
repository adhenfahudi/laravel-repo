<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Membuat CRUD Pada Laravel - www.malasngoding.com</title>
</head>
<body>
 
	<h2></h2>
	<h3>Data Pekerja</h3>
 
	<a href="pekerja/tambah"> + Tambah Pekerja Baru</a>
	
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>Nama</th>
			<th>Jabatan</th>
			<th>Umur</th>
			<th>Alamat</th>
			<th>Opsi</th>
		</tr>
		@foreach($pekerja as $p)
		<tr>
			<td>{{ $p->pekerja_nama }}</td>
			<td>{{ $p->pekerja_jabatan }}</td>
			<td>{{ $p->pekerja_umur }}</td>
			<td>{{ $p->pekerja_alamat }}</td>
			<td>
				<a href="pekerja/edit/{{ $p->pekerja_id }}">Edit</a>
				|
				<a href="pekerja/hapus/{{ $p->pekerja_id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
 
 
</body>
</html>