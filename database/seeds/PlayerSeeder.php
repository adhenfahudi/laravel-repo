<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PlayerSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // data faker indonesia
         $faker = Faker::create('id_ID');
 
         // membuat data dummy sebanyak 10 record
         for($x = 1; $x <= 10; $x++){
  
             // insert data dummy pekerja dengan faker
             DB::table('player')->insert([
                 'nama' => $faker->name,
                 'email'=> $faker->email,
                 'umur' => $faker->numberBetween(25,40),
                 'alamat' => $faker->address,
             ]);
  
       }
    }
}
