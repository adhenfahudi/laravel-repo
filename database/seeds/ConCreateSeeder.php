<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ConCreateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // data faker indonesia
         $faker = Faker::create('id_ID');
 
         // membuat data dummy sebanyak 10 record
         for($x = 1; $x <= 10; $x++){
  
             // insert data dummy pekerja dengan faker
             DB::table('con_creates')->insert([
                 'nama' => $faker->name,
                 'email'=> $faker->email,
                 'jabatan'=> $faker->jobTitle,
                 'alamat' => $faker->address,
             ]);
  
       }
    }
}
