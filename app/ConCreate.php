<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConCreate extends Model
{
    protected $table = 'con_creates';
    protected $fillable = ['nama', 'email', 'jabatan', 'alamat'];

    public function Team()
        {
    	return $this->belongsTo('App\Team');
    }
}
?>