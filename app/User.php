<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
 
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
 
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
 
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
 
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
// namespace App;
// // namespace Tymon\JWTAuth\Contracts;
// // use Illuminate\Notifications\Notifiable;
// // use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Database\Eloquent\Model;
// use Tymon\JWTAuth\Contracts\JWTSubject;

// class User extends Authenticatable implements JWTSubject
// {

//     protected $table = 'users';
//     protected $fillable = ['name', 'email', 'password'];
    
//     public function getJWTIdentifier()
//     {
//         return $this->getKey();
//     }

//     public function getJWTCustomClaims()
//     {
//         return [];
//     }
//     public function setPasswordAttribute($password)
//     {
//         if ( !empty($password) ) {
//             $this->attributes['password'] = bcrypt($password);
//         }
//     }  
    
//     // use Notifiable;

//     // /**
//     //  * The attributes that are mass assignable.
//     //  *
//     //  * @var array
//     //  */
//     // protected $fillable = [
//     //     'name', 'email', 'password',
//     // ];

//     // /**
//     //  * The attributes that should be hidden for arrays.
//     //  *
//     //  * @var array
//     //  */
//     // protected $hidden = [
//     //     'password', 'remember_token',
//     // ];


