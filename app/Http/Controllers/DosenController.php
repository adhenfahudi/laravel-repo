<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function index(){
        $nama = "Adhen Prawita Fahudi";
        $mapel = ["Algoritma dan Pemrograman","Kalkulus", "Pemrograman Web"];
        return view ('biodata', ['nama' => $nama, 'makul' => $mapel]);
        }
}
