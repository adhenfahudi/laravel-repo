<?php
 
namespace App\Http\Controllers;
 
use App\Pekerja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

 
class PekerjaController extends Controller
{
    public function index()
    {
    	// mengambil data dari table pegawai
		$pekerja = Pekerja::all();
		//return view('pekerja', ['pekerja' => $pekerja]);
		return Pekerja::all();
    }

    	// method untuk menampilkan view form tambah pekerja
    public function tambah()
    {
		// $pekerja = new Pekerja ;
		//  $pekerja->nama = $request->nama;
		//  $pekerja->umur = $request->umur;
		//  $pekerja->jabatan = $request->jabatan;
		//  $pekerja->alamat = $request->alamat;
		//  $pekerja->save();
	    // memanggil view tambah
	    //return view('pekerja_tambah');
    }

    	// method untuk insert data ke table pegawai
    public function store(Request $request)
    {
		// insert data ke table pegawai
		//DB::table('pegawai')->insert([
		//'pekerja_nama' => $request->nama,
		//'pekerja_jabatan' => $request->jabatan,
		//'pekerja_umur' => $request->umur,
		//'pekerja_alamat' => $request->alamat
		//]);
		// alihkan halaman ke halaman pegawai
		//return redirect('pekerja');

		$this->validate($request,[
			'nama' => 'required',
			'umur' => 'required',
			'jabatan' => 'required',
			'alamat' => 'required'
			]);
		Pekerja::create([
			'nama' => $request->nama,
			'umur' => $request->umur,
			'jabatan' => $request->jabatan,
			'alamat' => $request->alamat
			]);
		return response() ->json('Data Ditambahkan');
		//return redirect('pekerja');
		//return ('Data Berhasil di Rekam');
	}
	

   	 	// method untuk edit data pegawai
    	public function getid($id)
    {
		   $pekerja = Pekerja::find($id);
		   
		   //return view('pekerja_edit', ['pekerja' => $pekerja]);       
		   return response() ->json (Pekerja::find($id));
    }

    	// update data pegawai
    	public function update($id, Request $request)
    	{
		 $pekerja = Pekerja::find($id);
		 $pekerja->nama = $request->nama;
		 $pekerja->umur = $request->umur;
		 $pekerja->jabatan = $request->jabatan;
		 $pekerja->alamat = $request->alamat;
		 $pekerja->save();
		 //return redirect('pekerja');
		 return ('Data Berhasil Di Perbarui');
    	}

    	// method untuk hapus data pegawai
    public function delete($id)
    {
		// menghapus data pegawai berdasarkan id yang dipilih
	//DB::table('pekerja')->where('pekerja_id',$id)->delete();
		
		// alihkan halaman ke halaman pegawai
	//return redirect('pekerja');
	$pekerja = Pekerja::find($id);
    $pekerja->delete();
    //return redirect('pekerja');
	return ('Data Berhasil di delete');
    
    
}

}
