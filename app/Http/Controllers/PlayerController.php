<?php

namespace App\Http\Controllers;

use App\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function indexplayer()
    {
    	// mengambil data dari table pegawai
		$player = Player::all();
		//return view('pekerja', ['pekerja' => $pekerja]);
		return Player::all();
    }

    	// method untuk menampilkan view form tambah pekerja
    public function tambahplayer()
    {
		// $pekerja = new Pekerja ;
		//  $pekerja->nama = $request->nama;
		//  $pekerja->umur = $request->umur;
		//  $pekerja->jabatan = $request->jabatan;
		//  $pekerja->alamat = $request->alamat;
		//  $pekerja->save();
	    // memanggil view tambah
	    //return view('pekerja_tambah');
    }

    	// method untuk insert data ke table pegawai
    public function storeplayer(Request $request)
    {
		// insert data ke table pegawai
		//DB::table('pegawai')->insert([
		//'pekerja_nama' => $request->nama,
		//'pekerja_jabatan' => $request->jabatan,
		//'pekerja_umur' => $request->umur,
		//'pekerja_alamat' => $request->alamat
		//]);
		// alihkan halaman ke halaman pegawai
		//return redirect('pekerja');

		$this->validate($request,[
            'nama' => 'required',
            'email' => 'required',
			'umur' => 'required',
			'alamat' => 'required'
			]);
		Player::create([
            'nama' => $request->nama,
            'email' => $request->email,
			'umur' => $request->umur,
			'alamat' => $request->alamat
			]);
		return ('Data Ditambahkan');
		//return redirect('pekerja');
		//return ('Data Berhasil di Rekam');
	}
	

   	 	// method untuk edit data pegawai
    	public function getidplayer($id)
    {
		   $pekerja = Player::find($id);
		   
		   //return view('pekerja_edit', ['pekerja' => $pekerja]);       
		   return Player::find($id);
    }

    	// update data pegawai
    	public function updateplayer($id, Request $request)
    	{
		$this->validate($request,[
            'nama' => 'required',
            'email' => 'required',
			'umur' => 'required',
			'alamat' => 'required'
		 ]);
	  
		 $player = Player::find($id);
		 $player->nama = $request->nama;
         $player->email = $request->email;
         $player->umur = $request->umur;
		 $player->alamat = $request->alamat;
		 $player->save();

		 //return redirect('pekerja');
		 return ('Data Berhasil Di Perbarui');
    	}

    	// method untuk hapus data pegawai
    public function deleteplayer($id)
    {
		// menghapus data pegawai berdasarkan id yang dipilih
	//DB::table('pekerja')->where('pekerja_id',$id)->delete();
		
		// alihkan halaman ke halaman pegawai
	//return redirect('pekerja');
	$player = Player::find($id);
    $player->delete();
    //return redirect('pekerja');
	return ('Data Berhasil di delete');

    }
}