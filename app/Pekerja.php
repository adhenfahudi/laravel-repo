<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerja extends Model
{
    protected $table = 'pekerja';
    protected $fillable = ['nama', 'umur', 'jabatan', 'alamat'];

    public function team()
    {
    	return $this->belongsTo('App\Team');
    }
}
