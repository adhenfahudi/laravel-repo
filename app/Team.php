<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
   
    protected $table = 'teams';

    public function concreate()
    {
    	return $this->hasOne('App\ConCreate');
    }

    public function pekerja()
    {
        return $this->hasOne('App\Pekerja');
    }
   
    public function player()
    {
    	return $this->hasOne('App\Player');
    }

    public function division(){
    	return $this->hasMany('App\Division');
    }

  
}
