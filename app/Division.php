<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table = 'divisions';
    public function team()
    {
    	return $this->belongsTo('App\Team');
    }
}
