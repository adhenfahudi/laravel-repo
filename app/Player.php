<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'player';
    protected $fillable = ['nama', 'email', 'umur', 'alamat'];

    public function Team()
    {
    	return $this->belongsTo('App\Team');
    }
}
